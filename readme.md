yarn add express jsonwebtoken pg typeorm reflect-metadata

yarn add @types/express -D

yarn add ts-node-dev -D

yarn add typescript -D

yarn add eslint@6.8.0 -D

yarn tsc --init

Ir no arquivo tsconfig e descomentar outDir, rootDir, strictPropertyInitialization (false), experimentalDecorators e emitDecoratorMetadata

Instalar:

"@typescript-eslint/eslint-plugin": "^3.4.0",
"@typescript-eslint/parser": "^3.4.0",
"eslint-config-airbnb-base": "^14.2.0",
"eslint-config-prettier": "^6.11.0",
"eslint-import-resolver-typescript": "^2.0.0",
"eslint-plugin-import": "^2.21.2",
"eslint-plugin-prettier": "^3.1.4"

