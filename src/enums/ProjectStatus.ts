enum ProjectStatus {
  NEW = 'NEW',
  IN_PROGRESS = 'IN_PROGRESS',
  PAUSE = 'PAUSE',
  FINISHED = 'FINISHED',
  CANCELED = 'IN_PROGRESS',
}
export default ProjectStatus;
