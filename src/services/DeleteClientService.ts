import AppError from '../errors/AppError';
import InterfaceClientsRepository from '../repositories/InterfaceClientsRepository';

export default class DeleteClientService {
  private clientRepository: InterfaceClientsRepository;

  constructor(clientRepository: InterfaceClientsRepository) {
    this.clientRepository = clientRepository;
  }

  public async execute(id: string): Promise<void> {
    const client = await this.clientRepository.findById(id);

    if (!client) {
      throw new AppError('Cliente não existe', 400);
    }

    await this.clientRepository.delete(id);
  }
}
