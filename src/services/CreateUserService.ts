import { hash } from 'bcryptjs';
import InterfaceUser from '../repositories/InterfaceUserRepostitory';
import UserRepository from '../repositories/UserRepository';
import User from '../models/User';

interface Request {
  name: string;
  email: string;
  password: string;
}

export default class CreateUserService {
  private userRepository: InterfaceUser;

  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute({ name, email, password }: Request): Promise<User> {
    const passwordHash = await hash(password, 8);

    const user = await this.userRepository.create({
      name,
      email,
      password: passwordHash,
    });
    return user;
  }
}
