import InterfaceUser from '../repositories/InterfaceUserRepostitory';
import UserRepository from '../repositories/UserRepository';
import User from '../models/User';
import AppError from '../errors/AppError';

interface Request {
  id: string;
}

export default class EnableUserService {
  private userRepository: InterfaceUser;

  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute({ id }: Request): Promise<User> {
    const user = await this.userRepository.findById(id);

    if (!user) {
      throw new AppError('Usuário não encontrado', 404);
    }
    user.active = !user.active;

    await this.userRepository.save(user);

    return user;
  }
}
