import CreateClientDTO from '../dtos/CreateClientDTO';
import AppError from '../errors/AppError';
import Client from '../models/Client';
import InterfaceClientsRepository from '../repositories/InterfaceClientsRepository';

export default class CreateClientService {
  private clientRepository: InterfaceClientsRepository;

  constructor(clientRepository: InterfaceClientsRepository) {
    this.clientRepository = clientRepository;
  }

  public async execute({
    name,
    email,
    telephone,
    cpf,
  }: CreateClientDTO): Promise<Client> {
    const verifyClient = await this.clientRepository.findByEmail(email);

    if (verifyClient) {
      throw new AppError('Cliente já existe', 400);
    }

    const client = await this.clientRepository.create({
      name,
      email,
      telephone,
      cpf,
    });
    return client;
  }
}
