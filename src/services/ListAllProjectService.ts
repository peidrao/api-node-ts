import Project from '../models/Project';
import IProjectRepository from '../repositories/InterfaceProjectsRepository';

export default class ListAllProjectService {
  private projectRepository: IProjectRepository;

  constructor(projectRepository: IProjectRepository) {
    this.projectRepository = projectRepository;
  }

  public async execute(): Promise<Project[]> {
    const projects = await this.projectRepository.findAll();
    return projects;
  }
}
