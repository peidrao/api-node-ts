import ProjectStatus from '../enums/ProjectStatus';
import AppError from '../errors/AppError';
import Project from '../models/Project';
import InterfaceClientsRepository from '../repositories/InterfaceClientsRepository';
import IProjectRepository from '../repositories/InterfaceProjectsRepository';

interface IRequest {
  name: string;
  client_id: string;
  description: string;
  logo: string;
}

export default class CreateProjectService {
  private projectRepository: IProjectRepository;

  private clientRepository: InterfaceClientsRepository;

  constructor(
    projectRepository: IProjectRepository,
    clientRepository: InterfaceClientsRepository,
  ) {
    this.projectRepository = projectRepository;
    this.clientRepository = clientRepository;
  }

  public async execute({
    name,
    client_id,
    description,
    logo,
  }: IRequest): Promise<Project> {
    const verifyClient = await this.clientRepository.findById(client_id);

    if (!verifyClient) {
      throw new AppError('Client não encontrado', 400);
    }
    const project = await this.projectRepository.create({
      name,
      client_id,
      description,
      logo,
      status: ProjectStatus.NEW,
    });
    return project;
  }
}
