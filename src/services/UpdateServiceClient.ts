import AppError from '../errors/AppError';
import Client from '../models/Client';
import InterfaceClientsRepository from '../repositories/InterfaceClientsRepository';

interface IRequest {
  id: string;
  name: string;
  email: string;
  telephone: string;
  cpf: string;
}
export default class UpdateClientService {
  private clientRepository: InterfaceClientsRepository;

  constructor(clientRepository: InterfaceClientsRepository) {
    this.clientRepository = clientRepository;
  }

  public async execute({
    id,
    name,
    email,
    telephone,
    cpf,
  }: IRequest): Promise<Client> {
    const client = await this.clientRepository.findById(id);

    if (!client) {
      throw new AppError('Cliente não encotrado', 400);
    }

    if (email !== client.email) {
      const verifyClientEmail = this.clientRepository.findByEmail(email);
      if (verifyClientEmail) {
        throw new AppError('E-email já está sendo usado', 400);
      }
    }

    client.name = name;
    client.email = email;
    client.telephone = telephone;
    client.cpf = cpf;
    await this.clientRepository.save(client);

    return client;
  }
}
