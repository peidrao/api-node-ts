import '../config/env';
import { compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import InterfaceUser from '../repositories/InterfaceUserRepostitory';
import UserRepository from '../repositories/UserRepository';
import User from '../models/User';
import AppError from '../errors/AppError';

interface Request {
  email: string;
  password: string;
}

interface Response {
  token: string;
  user: User;
}

export default class SessionService {
  private userRepository: InterfaceUser;

  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute({ email, password }: Request): Promise<Response> {
    const user = await this.userRepository.findByEmail(email);

    if (!user) {
      throw new AppError('E-mail inválido', 401);
    }

    const passwordCompare = await compare(password, user.password);

    if (!passwordCompare) {
      throw new AppError('Senha inválida', 401);
    }

    if (!user.active) {
      throw new AppError('Usuário desativado!', 401);
    }

    const token = sign({}, process.env.APP_SECRET as string, {
      expiresIn: '1d',
    });
    // delete user.password;

    return { token, user };
  }
}
