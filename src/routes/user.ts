import { Router } from 'express';
import UsercController from '../controllers/UserController';
import authenticate from '../middlewares/auth';

const userRoutes = Router();
const userController = new UsercController();

userRoutes.post('/', userController.create);

userRoutes.patch('/:id', authenticate, userController.enable);

export default userRoutes;
