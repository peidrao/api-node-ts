import { Router } from 'express';
import SessionControler from '../controllers/SessionControler';

const sessionRoutes = Router();
const sessionController = new SessionControler();

sessionRoutes.post('/', sessionController.create);

export default sessionRoutes;
