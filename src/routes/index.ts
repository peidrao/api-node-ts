import { Router } from 'express';
import userRoutes from './user';
import sessionRoutes from './session';
import clientRoutes from './client';

const prefixRoutes = '/api/v1';

const routes = Router();

routes.use(`${prefixRoutes}/users`, userRoutes);
routes.use(`${prefixRoutes}/session`, sessionRoutes);
routes.use(`${prefixRoutes}/client`, clientRoutes);

export default routes;
