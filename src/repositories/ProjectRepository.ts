import { getRepository, Repository } from 'typeorm';
import CreateProjectDTO from '../dtos/CreateProjectDTO';
import Project from '../models/Project';
import IProjectRepository from './InterfaceProjectsRepository';

export default class ProjectRepository implements IProjectRepository {
  private ormRepository: Repository<Project>;

  constructor() {
    this.ormRepository = getRepository(Project);
  }

  public async findAll(): Promise<Project[]> {
    return this.ormRepository.find({ relations: ['client'] });
  }

  public async findById(id: string): Promise<Project | undefined> {
    const project = await this.ormRepository.findOne(id, {
      relations: ['client'],
    });
    return project;
  }

  public async create({
    name,
    client_id,
    description,
    logo,
    status,
  }: CreateProjectDTO): Promise<Project> {
    const project = this.ormRepository.create({
      name,
      client_id,
      description,
      logo,
      status,
    });
    await this.ormRepository.save(project);
    return project;
  }

  public async save(project: Project): Promise<Project> {
    return this.ormRepository.save(project);
  }
}
