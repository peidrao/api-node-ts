import { getRepository, Like, Repository } from 'typeorm';
import CreateClientDTO from '../dtos/CreateClientDTO';
import Client from '../models/Client';
import InterfaceClientsRepository from './InterfaceClientsRepository';

export default class ClientRepository implements InterfaceClientsRepository {
  private ormRepository: Repository<Client>;

  constructor() {
    this.ormRepository = getRepository(Client);
  }

  public async findAllByName(name: string): Promise<Client[]> {
    return this.ormRepository.find({
      name: Like(`%${name}%`),
    });
  }

  public async findAllPaginated(page: number): Promise<[Client[], number]> {
    return this.ormRepository.findAndCount({ skip: page, take: 10 });
  }

  public findAll(): Promise<Client[]> {
    return this.ormRepository.find();
  }

  public async findById(id: string): Promise<Client | undefined> {
    const client = await this.ormRepository.findOne({ where: { id } });
    return client;
  }

  public async findByEmail(email: string): Promise<Client | undefined> {
    const client = await this.ormRepository.findOne({ where: { email } });
    return client;
  }

  public async create({
    name,
    email,
    cpf,
    telephone,
  }: CreateClientDTO): Promise<Client> {
    const client = this.ormRepository.create({
      name,
      email,
      cpf,
      telephone,
    });

    await this.ormRepository.save(client);
    return client;
  }

  public save(client: Client): Promise<Client> {
    return this.ormRepository.save(client);
  }

  public async delete(id: string): Promise<void> {
    this.ormRepository.delete(id);
  }
}
