import CreateProjectDTO from '../dtos/CreateProjectDTO';
import Project from '../models/Project';

export default interface IProjectRepository {
  findAll(): Promise<Project[]>;
  findById(id: string): Promise<Project | undefined>;
  create(createProject: CreateProjectDTO): Promise<Project>;
  save(project: Project): Promise<Project>;
}
