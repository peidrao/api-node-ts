import { getRepository, Repository } from 'typeorm';
import User from '../models/User';
import InterfaceUserRepository from './InterfaceUserRepostitory';
import CreateUserDTO from '../dtos/CreateUserDTO';

export default class UserRepository implements InterfaceUserRepository {
  private ormRepository: Repository<User>;

  constructor() {
    this.ormRepository = getRepository(User);
  }

  public async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.ormRepository.findOne({ where: { email } });
    return user;
  }

  public async create({ name, email, password }: CreateUserDTO): Promise<User> {
    const user = this.ormRepository.create({ name, email, password });
    await this.ormRepository.save(user);
    return user;
  }

  public async findById(id: string): Promise<User | undefined> {
    return this.ormRepository.findOne({ where: { id } });
  }

  public async save(user: User): Promise<User> {
    return this.ormRepository.save(user);
  }
}
