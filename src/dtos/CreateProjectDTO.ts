import ProjectStatus from '../enums/ProjectStatus';

export default interface CreateProjectDTO {
  name: string;
  client_id: string;
  description: string;
  logo: string;
  status: ProjectStatus;
}
