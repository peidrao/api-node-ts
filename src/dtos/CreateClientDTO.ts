export default interface CreateClientDTO {
  name: string;
  telephone: string;
  email: string;
  cpf: string;
}
